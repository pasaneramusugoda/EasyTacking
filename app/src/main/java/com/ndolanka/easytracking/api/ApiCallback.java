package com.ndolanka.easytracking.api;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Pasan on 10/4/2016.
 */

public interface ApiCallback<T> {
    /**
     * Called for [200, 300) responses.
     */
    void success(int code, Response<T> response);

    /**
     * Called for 401 responses (unauthenticated).
     * Called for [400, 500) responses, except 401 (clientError).
     * Called for [500, 600) response (serverError).
     * Called for network errors while making the call (networkError).
     * Called for unexpected errors while making the call (unexpectedError).
     */
    void failed(int code, Response<?> response, Throwable t);

    /**
     * Called at last
     *
     * @param call
     */
    void atLast(Call<?> call);
}
