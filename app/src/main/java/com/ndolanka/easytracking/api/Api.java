package com.ndolanka.easytracking.api;

import com.ndolanka.easytracking.models.AllocateModel;
import com.ndolanka.easytracking.models.JobDetailModel;
import com.ndolanka.easytracking.models.OngoingJobModel;
import com.ndolanka.easytracking.models.UserModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Pasan on 10/4/2016.
 */

interface Api {
    @FormUrlEncoded
    @POST("user_login")
    Call<UserModel> Login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("device_id") String device_id,
            @Field("os") String os,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );

    @FormUrlEncoded
    @POST("update_device_id")
    Call<ResponseBody> UpdateDeviceId(
            @Field("user_id") String username,
            @Field("user_type") String user_type,
            @Field("old_device_id") String old_device_id,
            @Field("device_id") String device_id,
            @Field("os") String os,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );

    @FormUrlEncoded
    @POST("allocate_job")
    Call<ResponseBody> AllocateJob(
            @Field("user_id") String username,
            @Field("job_id") String job_id,
            @Field("allocated_to_id") String allocated_to_id,
            @Field("old_allocated_to_id") String old_allocated_to_id,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );

    @GET("get_wharf_list/timestamp/{timestamp}/api_key/{api_key}")
    Call<AllocateModel> GetWharfList(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key
    );

    @GET("get_jobs_to_allocate/timestamp/{timestamp}/api_key/{api_key}")
    Call<OngoingJobModel> GetJobsToAllocate(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key
    );

    @GET("wharf_get_ongoin_jobs/timestamp/{timestamp}/api_key/{api_key}/user_id/{user_id}/user_type/{user_type}")
    Call<OngoingJobModel> GetOngoingJobs(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key,
            @Path("user_id") String user_id,
            @Path("user_type") String user_type
    );

    @GET("wharf_get_today_completed_jobs/timestamp/{timestamp}/api_key/{api_key}/user_id/{user_id}/user_type/{user_type}")
    Call<OngoingJobModel> GetTodayCompletedJobs(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key,
            @Path("user_id") String user_id,
            @Path("user_type") String user_type
    );

    @GET("wharf_get_all_completed_jobs/timestamp/{timestamp}/api_key/{api_key}/user_id/{user_id}/user_type/{user_type}/skip/{skip}/take/{take}")
    Call<OngoingJobModel> GetAllCompletedJobs(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key,
            @Path("user_id") String user_id,
            @Path("user_type") String user_type,
            @Path("skip") String skip,
            @Path("take") String take
    );

    @GET("get_job_detail/timestamp/{timestamp}/api_key/{api_key}/job_id/{job_id}/user_type/{user_type}")
    Call<JobDetailModel> GetJobDetail(
            @Path("timestamp") String timestamp,
            @Path("api_key") String api_key,
            @Path("job_id") String job_id,
            @Path("user_type") String user_type
    );

    @FormUrlEncoded
    @POST("update_job_status")
    Call<JobDetailModel> UpdateJobStatus(
            @Field("user_id") String user_id,
            @Field("job_id") String job_id,
            @Field("milestone_id") String milestone_id,
            @Field("completed") String completed,
            @Field("remarks") String remarks,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );

    @FormUrlEncoded
    @POST("user_logout")
    Call<ResponseBody> Logout(
            @Field("user_id") String user_id,
            @Field("user_type") String user_type,
            @Field("device_id") String device_id,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );

    @FormUrlEncoded
    @POST("change_password")
    Call<ResponseBody> ChangePassword(
            @Field("username") String username,
            @Field("old_password") String old_password,
            @Field("new_password") String password,
            @Field("timestamp") String timestamp,
            @Field("api_key") String api_key
    );
}
