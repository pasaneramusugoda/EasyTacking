package com.ndolanka.easytracking.api;

import android.content.Context;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.ApiKeyGen;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.models.AllocateModel;
import com.ndolanka.easytracking.models.JobDetailModel;
import com.ndolanka.easytracking.models.OngoingJobModel;
import com.ndolanka.easytracking.models.UserModel;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pasan on 10/4/2016.
 */

public class ApiUsage {
    private static final String TAG = ApiUsage.class.getSimpleName();
    private static Api mApi;
    private static ApiKeyGen mKeyGen;

    public static void initialize(Context context) {
        new ApiUsage(context);
    }

    private ApiUsage(Context context) {
        String BASE_URL = Functions.GetStringResource(context, R.string.service_base_url);

        mKeyGen = new ApiKeyGen(context);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

        Retrofit mClient = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        mApi = mClient.create(Api.class);
    }

    public static Call<UserModel> Login(String deviceId, String username, String password, final ApiCallback<UserModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<UserModel> call = mApi.Login(
                username, password, deviceId, "A", mKeyGen.getTimeStamp(), mKeyGen.getKey()
        );
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<ResponseBody> UpdateDeviceId(String userId, String userType, String oldDeviceId, String deviceId, final ApiCallback<ResponseBody> responseBodyCallback) {
        mKeyGen.generate();
        Call<ResponseBody> call = mApi.UpdateDeviceId(
                userId, userType, oldDeviceId, deviceId, "A", mKeyGen.getTimeStamp(), mKeyGen.getKey()
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<ResponseBody> AllocateJob(String userId, String jobId, String allocatedId, String oldAllocatedId, final ApiCallback<ResponseBody> responseBodyCallback) {
        mKeyGen.generate();
        Call<ResponseBody> call = mApi.AllocateJob(
                userId, jobId, allocatedId, oldAllocatedId, mKeyGen.getTimeStamp(), mKeyGen.getKey()
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<AllocateModel> GetWharfList(final ApiCallback<AllocateModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<AllocateModel> call = mApi.GetWharfList(mKeyGen.getTimeStamp(), mKeyGen.getKey());
        call.enqueue(new Callback<AllocateModel>() {
            @Override
            public void onResponse(Call<AllocateModel> call, Response<AllocateModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<AllocateModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<OngoingJobModel> GetJobsToAllocate(final ApiCallback<OngoingJobModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<OngoingJobModel> call = mApi.GetJobsToAllocate(mKeyGen.getTimeStamp(), mKeyGen.getKey());
        call.enqueue(new Callback<OngoingJobModel>() {
            @Override
            public void onResponse(Call<OngoingJobModel> call, Response<OngoingJobModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<OngoingJobModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<OngoingJobModel> GetOngoingJobs(String userId, String userType, final ApiCallback<OngoingJobModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<OngoingJobModel> call = mApi.GetOngoingJobs(mKeyGen.getTimeStamp(), mKeyGen.getKey(), userId, userType);
        call.enqueue(new Callback<OngoingJobModel>() {
            @Override
            public void onResponse(Call<OngoingJobModel> call, Response<OngoingJobModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<OngoingJobModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<OngoingJobModel> GetTodayCompletedJobs(String userId, String userType, final ApiCallback<OngoingJobModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<OngoingJobModel> call = mApi.GetTodayCompletedJobs(mKeyGen.getTimeStamp(), mKeyGen.getKey(), userId, userType);
        call.enqueue(new Callback<OngoingJobModel>() {
            @Override
            public void onResponse(Call<OngoingJobModel> call, Response<OngoingJobModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<OngoingJobModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<OngoingJobModel> GetAllCompletedJobs(String userId, String userType, String skip, String take, final ApiCallback<OngoingJobModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<OngoingJobModel> call = mApi.GetAllCompletedJobs(mKeyGen.getTimeStamp(), mKeyGen.getKey(), userId, userType, skip, take);
        call.enqueue(new Callback<OngoingJobModel>() {
            @Override
            public void onResponse(Call<OngoingJobModel> call, Response<OngoingJobModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<OngoingJobModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<JobDetailModel> GetJobDetail(String jobId, String userType, final ApiCallback<JobDetailModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<JobDetailModel> call = mApi.GetJobDetail(mKeyGen.getTimeStamp(), mKeyGen.getKey(), jobId, userType);
        call.enqueue(new Callback<JobDetailModel>() {
            @Override
            public void onResponse(Call<JobDetailModel> call, Response<JobDetailModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<JobDetailModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<JobDetailModel> UpdateJobStatus(String userId, String jobId, String milestoneId,
                                                       String completed, String remarks,
                                                       final ApiCallback<JobDetailModel> responseBodyCallback) {
        mKeyGen.generate();
        Call<JobDetailModel> call = mApi.UpdateJobStatus(userId, jobId, milestoneId, completed, remarks,
                mKeyGen.getTimeStamp(), mKeyGen.getKey());
        call.enqueue(new Callback<JobDetailModel>() {
            @Override
            public void onResponse(Call<JobDetailModel> call, Response<JobDetailModel> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<JobDetailModel> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<ResponseBody> Logout(String userId, String userType, String deviceId,
                                            final ApiCallback<ResponseBody> responseBodyCallback) {
        mKeyGen.generate();
        Call<ResponseBody> call = mApi.Logout(userId, userType, deviceId,
                mKeyGen.getTimeStamp(), mKeyGen.getKey());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }

    public static Call<ResponseBody> ChangePassword(String username, String oldPassword, String password,
                                                    final ApiCallback<ResponseBody> responseBodyCallback) {
        mKeyGen.generate();
        Call<ResponseBody> call = mApi.ChangePassword(username, oldPassword, password, mKeyGen.getTimeStamp(), mKeyGen.getKey());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, call.request().url().toString());
                int code = response.code();
                if (code >= 200 && code < 300) {
                    responseBodyCallback.success(code, response);
                } else if ((code == 401) || (code >= 400 && code < 500) || (code >= 500 && code < 600)) {
                    responseBodyCallback.failed(code, response, null);
                } else {
                    responseBodyCallback.failed(0, response, new RuntimeException("Unexpected response " + response));
                }

                responseBodyCallback.atLast(call);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (call.isCanceled()) {
                    Log.d(TAG, String.format("Request Cancelled : %s", call.request().url().toString()));
                } else {
                    Log.e(TAG, call.request().url().toString());
                    Log.e(TAG, t.toString());
                    responseBodyCallback.failed(0, null, t);
                }
                responseBodyCallback.atLast(call);
            }
        });
        return call;
    }
}
