package com.ndolanka.easytracking.fcm;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Pasan on 11/4/2016.
 */

public class AppFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = AppFirebaseInstanceIDService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        User user = Preference.getInstance(getApplicationContext()).getCurrentUser();
        if (user != null && user.getLogged()) {
            ApiUsage.UpdateDeviceId(user.getId(), user.getUserType(), user.getDeviceId(), token, new ApiCallback<ResponseBody>() {
                @Override
                public void success(int code, Response<ResponseBody> response) {
                    try {
                        String jsonString = response.body().string();
                        Log.d(TAG, "UpdateDeviceId", jsonString);
                        JSONObject object = new JSONObject(jsonString);

                        if (object.getBoolean("result")) {
                            Log.d(TAG, "UpdateDeviceId", "success");
                            writeStatus(false);
                        } else {
                            writeStatus(true);
                        }
                    } catch (IOException | JSONException e) {
                        FirebaseCrash.report(e);
                        Log.e(TAG, "UpdateDeviceId", e.getMessage());
                        writeStatus(true);
                    }
                }

                @Override
                public void failed(int code, Response<?> response, Throwable t) {
                    String msg = "Code: " + String.valueOf(code);
                    if (response != null) {
                        msg += " Response: " + response.message();
                    } else if (t != null) {
                        msg += " Error: " + t.getMessage();
                    }

                    Log.e(TAG, "UpdateDeviceId", msg, t);
                    writeStatus(true);
                }

                @Override
                public void atLast(Call<?> call) {

                }
            });
        }
    }

    private void writeStatus(boolean status) {
        Preference.getInstance(getApplicationContext()).write(Preference.Keys.TOKEN_REFRESH_NEEDED, status);
    }
}