package com.ndolanka.easytracking.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.activities.AllocateActivity;
import com.ndolanka.easytracking.views.activities.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

/**
 * Created by Pasan on 11/4/2016.
 */

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = AppFirebaseMessagingService.class.getSimpleName();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getNotification() != null
                && remoteMessage.getNotification().getBody() != null) {
            Log.d(TAG, "FCM Notification Message Body: " + remoteMessage.getNotification().getBody());
        }

        if (!remoteMessage.getData().isEmpty()) {
            Log.d(TAG, "Notification Message Data: " + remoteMessage.getData().toString());
            handleNotification(remoteMessage.getData());
        }
    }
    // [END receive_message]

    private void handleNotification(Map<String, String> data) {
        final Context context = this;

        final User user = Preference.getInstance(getApplicationContext()).getCurrentUser();

        if (user == null || !user.getLogged())
            return;

        try {
            JSONObject notification = new JSONObject(data);

            String message = notification.getString("message");
            Intent intent;

            switch (notification.getInt("notification_type")) {
                case 1:
                    intent = new Intent(context, MainActivity.class);
                    break;
                case 2:
                    intent = new Intent(context, AllocateActivity.class);
                    break;
                default:
                    intent = new Intent(context, MainActivity.class);
                    break;
            }

            intent.putExtra(MainActivity.EXTRA_NEW_ALLOCATION, notification.toString());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            sendNotification(message, intent);
        } catch (JSONException e) {
            Log.e(TAG, "handleNotification", e.getMessage(), e);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(String message, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(String.format("Message from %s", getString(R.string.app_name)))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(generateRandom(), notificationBuilder.build());
    }

    public int generateRandom() {
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}