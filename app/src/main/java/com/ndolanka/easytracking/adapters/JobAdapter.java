package com.ndolanka.easytracking.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.interfaces.OnAdapterItemClickListener;
import com.ndolanka.easytracking.models.Job;
import com.ndolanka.easytracking.views.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pasan on 11/2/2016.
 */

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.ViewHolder> {

    private Context mContext;
    private List<Job> mJobs;
    private OnAdapterItemClickListener mListener;

    public JobAdapter(Context mContext, List<Job> mJobs, OnAdapterItemClickListener mListener) {
        this.mContext = mContext;
        this.mJobs = mJobs;
        this.mListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_job_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setVisibility(View.GONE);

        final Job job = mJobs.get(position);

        holder.mTClientName.setText(job.getConsignee());
        holder.mTHblNumber.setText(job.getHblNo());
        holder.mTJobNumber.setText(job.getJobNo());
        holder.mTOrderRefNumber.setText(job.getOrderReference());
        holder.mTDescription.setText(job.getDesc());

        if (job.getClosed().equals("1")) {
            holder.mVStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusClosed));
            holder.mTStatus.setText(R.string.text_job_status_closed);
            holder.mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusClosed));
        } else if (job.getClosed().equals("0") && job.getCurrentStatusId() != null) {
            holder.mVStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusOngoing));
            holder.mTStatus.setText(job.getCurrentStatus() != null ? String.valueOf(job.getCurrentStatus()) : "Ongoing");
            holder.mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusOngoing));
        } else if (job.getClosed().equals("0") && job.getCurrentStatusId() == null) {
            holder.mVStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusOpen));
            holder.mTStatus.setText(R.string.text_job_status_open);
            holder.mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusOpen));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(view, position, job);
                }
            }
        });

        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mJobs == null ? 0 : mJobs.size();
    }

    public void setFilter(List<Job> jobs) {
        mJobs = new ArrayList<>();
        mJobs.addAll(jobs);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_client_name)
        TextView mTClientName;
        @BindView(R.id.text_hbl_number)
        TextView mTHblNumber;
        @BindView(R.id.text_job_number)
        TextView mTJobNumber;
        @BindView(R.id.text_order_ref_number)
        TextView mTOrderRefNumber;
        @BindView(R.id.view_status_color_bar)
        View mVStatusColorBar;
        @BindView(R.id.text_status)
        TextView mTStatus;
        @BindView(R.id.text_description)
        TextView mTDescription;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
