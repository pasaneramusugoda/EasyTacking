package com.ndolanka.easytracking.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.models.TabItem;
import com.ndolanka.easytracking.views.fragments.TabItemFragment;
import com.ndolanka.easytracking.views.widget.TextView;

import java.util.List;

/**
 * Created by Pasan on 11/2/2016.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private List<TabItem> tabs;
    private Context context;

    public MainPagerAdapter(FragmentManager fm, Context context, List<TabItem> tabs) {
        super(fm);
        this.context = context;
        this.tabs = tabs;
    }

    @Override
    public int getCount() {
        return tabs == null ? 0 : tabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        return TabItemFragment.newInstance(position + 1);
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.layout_tab_item, null);
        TextView tv = (TextView) v.findViewById(R.id.text_tab_title);
        tv.setText(tabs.get(position).getTitle());
        TextView count = (TextView) v.findViewById(R.id.text_count);
        RelativeLayout countBg = (RelativeLayout) v.findViewById(R.id.text_count_bg);

        if (tabs.get(position).getCount() == 0) {
            countBg.setVisibility(View.GONE);
        } else {
            count.setText(String.format("%s", tabs.get(position).getCount()));
        }

        return v;
    }
}
