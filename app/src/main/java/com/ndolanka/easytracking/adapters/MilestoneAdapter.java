package com.ndolanka.easytracking.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.interfaces.OnAdapterItemClickListener;
import com.ndolanka.easytracking.models.Milestone;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.widget.Button;
import com.ndolanka.easytracking.views.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pasan on 11/2/2016.
 */

public class MilestoneAdapter extends RecyclerView.Adapter<MilestoneAdapter.ViewHolder> {

    private Context mContext;
    private User mUser;
    private List<Milestone> mMilestones;
    private OnAdapterItemClickListener mListener;

    public MilestoneAdapter(Context mContext, User mUser, List<Milestone> milestones, OnAdapterItemClickListener mListener) {
        this.mContext = mContext;
        this.mUser = mUser;
        this.mMilestones = milestones;
        this.mListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_milestone_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.itemView.setVisibility(View.GONE);

        final Milestone milestone = mMilestones.get(position);

        holder.mTextMilestone.setText(milestone.getMilestone());

        if (milestone.getRemarks() != null && !milestone.getRemarks().isEmpty())
            holder.mTextRemark.setText(milestone.getRemarks());
        else
            holder.mTextRemark.setVisibility(View.GONE);

        if (shouldDisable(position)) {
            holder.mButtonDelayed.setEnabled(false);
            holder.mButtonCompleted.setEnabled(false);
            holder.mViewStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusOpen));

            if (!mUser.getUserType().toLowerCase().equals("c"))
                holder.mViewButtonBar.setVisibility(View.VISIBLE);

            holder.mViewInfoBar.setVisibility(View.GONE);
        } else if (milestone.getCompleted() != null && milestone.getCompleted().equals("1")) {
            holder.mButtonDelayed.setEnabled(false);
            holder.mButtonCompleted.setEnabled(false);
            holder.mViewStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusClosed));
            holder.mViewButtonBar.setVisibility(View.GONE);
            holder.mViewInfoBar.setVisibility(View.VISIBLE);

            holder.mTextCompletedBy.setText(String.format("By %s", milestone.getCompletedBy()));
            holder.mTextCompletedOn.setText(String.format("Completed on %s", android.text.format.DateFormat.format("dd MMM yyyy hh:mm a", Functions.GetDateTime(milestone.getCompletedOn(), "yyyy-MM-dd HH:mm:ss"))));
        } else {
            holder.mViewStatusColorBar.setBackgroundColor(Functions.GetColor(mContext, R.color.colorStatusOngoing));
            holder.mButtonDelayed.setEnabled(true);
            holder.mButtonCompleted.setEnabled(true);

            if (!mUser.getUserType().toLowerCase().equals("c"))
                holder.mViewButtonBar.setVisibility(View.VISIBLE);

            holder.mViewInfoBar.setVisibility(View.GONE);

            holder.mButtonDelayed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemClick(view, position, milestone);
                    }
                }
            });

            holder.mButtonCompleted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onItemClick(view, position, milestone);
                    }
                }
            });
        }

        holder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mMilestones == null ? 0 : mMilestones.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_milestone)
        TextView mTextMilestone;
        @BindView(R.id.view_status_color_bar)
        View mViewStatusColorBar;
        @BindView(R.id.view_button_bar)
        View mViewButtonBar;
        @BindView(R.id.button_delayed)
        Button mButtonDelayed;
        @BindView(R.id.button_completed)
        Button mButtonCompleted;
        @BindView(R.id.view_info_bar)
        View mViewInfoBar;
        @BindView(R.id.text_completed_on)
        TextView mTextCompletedOn;
        @BindView(R.id.text_completed_by)
        TextView mTextCompletedBy;
        @BindView(R.id.text_remark)
        TextView mTextRemark;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    //region imp

    private boolean shouldDisable(int currentPosition) {
        if (mMilestones != null && !mMilestones.isEmpty()) {
            for (int i = currentPosition - 1; i >= 0; i--) {
                Milestone milestone = mMilestones.get(i);
                if (milestone.getCompleted() == null || milestone.getCompleted().equals("0")) {
                    return true;
                }
            }
        }
        return false;
    }

    //endregion
}
