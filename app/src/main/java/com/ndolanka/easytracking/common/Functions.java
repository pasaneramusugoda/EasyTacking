package com.ndolanka.easytracking.common;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.views.widget.ETValidator;
import com.ndolanka.easytracking.views.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pasan Eramusugoda on 4/8/2016.
 */
public class Functions {
    private static final String TAG = Functions.class.getSimpleName();

    /**
     * @param i
     * @return -2 = Bamini_Plain.ttf,
     * -1 = DL_Paras.TTF,
     * 0 = SFUIDisplay-Light.ttf,
     * 1 = SFUIDisplay-Regular.ttf
     */
    public static String GetFontPath(int i) {
        switch (i) {
            case 1:
                return "fonts/OpenSans_Bold.ttf";
            case 2:
                return "fonts/OpenSans_BoldItalic.ttf";
            case 3:
                return "fonts/OpenSans_ExtraBold.ttf";
            case 4:
                return "fonts/OpenSans_ExtraBoldItalic.ttf";
            case 5:
                return "fonts/OpenSans_Italic.ttf";
            case 6:
                return "fonts/OpenSans_Light.ttf";
            case 7:
                return "fonts/OpenSans_LightItalic.ttf";
            case 8:
                return "fonts/OpenSans_Semibold.ttf";
            case 9:
                return "fonts/OpenSans_SemiboldItalic.ttf";
            case 0:
            default:
                return "fonts/OpenSans_Regular.ttf";
        }
    }

    public static boolean IsInternetOn(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected();
    }

    public static int GetColor(Context context, int color_resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(color_resource);
        } else {
            return context.getResources().getColor(color_resource);
        }
    }

    public static Float GetDimension(Context context, int int_resource) {
        return context.getResources().getDimension(int_resource);
    }

    public static Drawable GetDrawable(Context context, int drawable_resource) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(drawable_resource);
        } else {
            return context.getResources().getDrawable(drawable_resource);
        }
    }

    public static String GetStringResource(Context mContext, int mResourceId) {
        return mContext.getResources().getString(mResourceId);
    }

    public static String GetErrorMessage(@Nullable String message, int StatusCode, Throwable e, JSONObject errorResponse) {
        StringBuilder msg = new StringBuilder(message == null || message.isEmpty() ? "Please retry." : message);

        msg.append("\n");
        msg.append("\n");
        msg.append("Status Code: ").append(String.valueOf(StatusCode));
        if (e != null && e.getMessage() != null) {
            msg.append("\n");
            msg.append("Error: ").append(e.getMessage());
        }
        if (errorResponse != null && errorResponse.has("msg")) {
            try {
                msg.append("\n");
                msg.append("Error Response: ").append(errorResponse.getString("msg"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        return msg.toString();
    }

    public static int GetToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static String GetPrice(double price) {
        return NumberFormat.getNumberInstance(Locale.US).format(price);
    }

    public static void HideKeyboard(Context context, View currentFoucs) {
        if (currentFoucs != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(currentFoucs.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * Encodes the byte array into base64 string
     *
     * @param byteArray - byte array
     * @return String a {@link java.lang.String}
     */
    public static String EncodeBase64(byte[] byteArray) {
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

    /**
     * Decodes the base64 string into byte array
     *
     * @param dataString - a {@link java.lang.String}
     * @return byte array
     */
    public static byte[] DecodeBase64(String dataString) {
        return Base64.decode(dataString, Base64.NO_WRAP);
    }

    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list =
                packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    //region date functions
    public static Date GetDateTime(String dateTime, String dateTimeFormat) {
        try {
            if (dateTimeFormat.isEmpty())
                dateTimeFormat = "yyyy-MM-dd";

            SimpleDateFormat retDateFormat = new SimpleDateFormat(dateTimeFormat);
            retDateFormat.setTimeZone(TimeZone.getDefault());

            return retDateFormat.parse(dateTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static long GetDateTimeDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        Log.d(TAG, "startDate : " + startDate);
        Log.d(TAG, "endDate : " + endDate);
        Log.d(TAG, "different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.d(TAG, String.format(Locale.getDefault(),
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds));
        //return different;
        return elapsedMinutes;
    }
    //endregion

    //region animation

    //    public static void AnimateView(@NonNull Techniques techniques, @NonNull View view, int duration) {
    //        YoYo.with(techniques)
    //                .duration(duration)
    //                .playOn(view);
    //    }

    //endregion

    //region validation functions
    public static boolean ValidateFields(EditText... fields) {
        if (fields.length == 0)
            return false;

        boolean result = true;
        for (EditText field :
                fields) {
            if (!field.validate())
                result = false;
        }

        return result;
    }

    public static ETValidator ValidationRequired(Context mContext) {
        return new ETValidator(GetStringResource(mContext, R.string.error_required_field)) {
            @Override
            public boolean isValid(CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() > 0;
            }
        };
    }

    public static ETValidator ValidationMin(Context mContext, final int value) {
        return new ETValidator(String.format(Locale.getDefault(), GetStringResource(mContext, R.string.error_min_length), value)) {
            @Override
            public boolean isValid(CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() >= value;
            }
        };
    }

    public static ETValidator ValidationMax(Context mContext, final int value) {
        return new ETValidator(String.format(Locale.getDefault(), GetStringResource(mContext, R.string.error_max_length), value)) {
            @Override
            public boolean isValid(CharSequence text, boolean isEmpty) {
                return !isEmpty && text.length() <= value;
            }
        };
    }

    public static ETValidator ValidationNIC(Context mContext) {
        return new ETValidator(Functions.GetStringResource(mContext, R.string.error_invalid_nic)) {
            @Override
            public boolean isValid(CharSequence text, boolean isEmpty) {
                return !isEmpty && IsValidNIC(text);
            }
        };
    }

    public static ETValidator ValidationEmail(Context mContext) {
        return new ETValidator(Functions.GetStringResource(mContext, R.string.error_invalid_email)) {
            @Override
            public boolean isValid(CharSequence text, boolean isEmpty) {
                return !isEmpty && IsValidEmail(text);
            }
        };
    }

    public static boolean IsValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean IsValidNIC(CharSequence target) {
        if (target != null && !target.toString().isEmpty()) {
            Pattern pattern = Pattern.compile("[0-9]{9}[vV]");
            Matcher matcher = pattern.matcher(target);
            return matcher.find();
        } else {
            return false;
        }
    }

    public static boolean IsValidPassword(CharSequence pass, CharSequence c_pass) {
        return !(pass == null || c_pass == null) && pass.toString().equals(c_pass.toString());
    }

    //endregion
}
