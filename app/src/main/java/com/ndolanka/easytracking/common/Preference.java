package com.ndolanka.easytracking.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ndolanka.easytracking.models.User;

import java.lang.reflect.Type;

/**
 * Created by Pasan Eramusugoda on 4/15/2016.
 */
public class Preference {
    protected static final String TAG = Preference.class.getSimpleName();
    private Context mContext;

    Preference(Context context) {
        mContext = context;
    }

    public static Preference getInstance(Context context) {
        return new Preference(context);
    }

    public void write(Keys preference, int value) {
        SharedPreferences.Editor mEditor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        mEditor.putInt(preference.name(), value);
        mEditor.apply();
        Log.d(TAG, "write: " + preference.name() + ":" + String.valueOf(value));
    }

    public void write(Keys preference, String value) {
        SharedPreferences.Editor mEditor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        mEditor.putString(preference.name(), value);
        mEditor.apply();
        Log.d(TAG, "write: " + preference.name() + ":" + String.valueOf(value));
    }

    public void write(Keys preference, Boolean value) {
        SharedPreferences.Editor mEditor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        mEditor.putBoolean(preference.name(), value);
        mEditor.apply();
        Log.d(TAG, "write: " + preference.name() + ":" + String.valueOf(value));
    }

    public int get(Keys preference, int default_value) {
        return (PreferenceManager.getDefaultSharedPreferences(mContext)).getInt(preference.name(), default_value);
    }

    public String get(Keys preference, String default_value) {
        return (PreferenceManager.getDefaultSharedPreferences(mContext)).getString(preference.name(), default_value);
    }

    public Boolean get(Keys preference, Boolean default_value) {
        return (PreferenceManager.getDefaultSharedPreferences(mContext)).getBoolean(preference.name(), default_value);
    }

    public User getCurrentUser() {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(mContext,
                Keys.CUSTOMER.name(), 0);
        User currentUser = complexPreferences.getObject(Keys.CURRENT_CUSTOMER.name(),
                User.class);
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(mContext,
                Keys.CUSTOMER.name(), 0);
        complexPreferences.putObject(Keys.CURRENT_CUSTOMER.name(), currentUser);
        complexPreferences.commit();
    }

    public void clearCurrentUser() {
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(mContext,
                Keys.CUSTOMER.name(), 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }

    /**
     * Created by Pasan Eramusugoda on 4/15/2016.
     */
    public enum Keys {
        DEVICE_ID, CUSTOMER_ID, TOKEN, TOKEN_EXPIRE_AT, CUSTOMER, CURRENT_CUSTOMER, DEAL_ID, TOKEN_REFRESH_NEEDED, CATEGORY
    }
}

class ComplexPreferences {

    private static Gson GSON = new Gson();
    Type typeOfObject = new TypeToken<Object>() {
    }.getType();
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private ComplexPreferences(Context context, String namePreferences, int mode) {
        if (namePreferences == null || namePreferences.equals("")) {
            namePreferences = "complex_preferences";
        }
        preferences = context.getSharedPreferences(namePreferences, mode);
        editor = preferences.edit();
    }

    public static ComplexPreferences getComplexPreferences(Context context,
                                                           String namePreferences, int mode) {
        return new ComplexPreferences(context, namePreferences, mode);
    }

    public void putObject(String key, Object object) {
        if (object == null) {
            throw new IllegalArgumentException("object is null");
        }

        if (key == null || key.equals("")) {
            throw new IllegalArgumentException("key is empty or null");
        }

        editor.putString(key, GSON.toJson(object));
    }

    public void commit() {
        editor.commit();
    }

    public void clearObject() {
        editor.clear();
    }

    public <T> T getObject(String key, Class<T> a) {

        String gson = preferences.getString(key, null);
        if (gson == null) {
            return null;
        } else {
            try {
                return GSON.fromJson(gson, a);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object storaged with key " + key + " is instanceof other class");
            }
        }
    }
}
