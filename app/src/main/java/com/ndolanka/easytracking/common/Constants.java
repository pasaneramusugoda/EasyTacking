package com.ndolanka.easytracking.common;

/**
 * Created by Pasan Eramusugoda on 5/31/2016.
 */

public final class Constants {
    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    private static final String PACKAGE_NAME = "com.ndolanka.easytracking";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    public static final String REQUEST = PACKAGE_NAME + ".REQUEST";

    public static final String PICKUP_LOCATION = PACKAGE_NAME + ".PICKUP_LOCATION";

    public static final String DROP_LOCATION = PACKAGE_NAME + ".DROP_LOCATION";
}
