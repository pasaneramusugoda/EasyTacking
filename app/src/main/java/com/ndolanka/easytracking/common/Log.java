package com.ndolanka.easytracking.common;


import com.ndolanka.easytracking.BuildConfig;

/**
 * Created by Pasan Eramusugoda on 4/12/2016.
 */
public class Log {
    public static void d(String tag, String msg) {
        d(tag, "", msg);
    }

    public static void d(String tag, String ref, String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.d(tag, ref.isEmpty() ? msg : ref + " - " + msg);
        }
    }

    public static void d(String tag, String ref, String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            android.util.Log.d(tag, ref.isEmpty() ? msg : ref + " - " + msg, tr);
        }
    }

    public static void e(String tag, String msg) {
        e(tag, "", msg);
    }

    public static void e(String tag, String ref, String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(tag, ref.isEmpty() ? msg : ref + " - " + msg);
        }
    }

    public static void e(String tag, String ref, String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(tag, ref.isEmpty() ? msg : ref + " - " + msg, tr);
        }
    }

    public static void w(String tag, String msg) {
        e(tag, "", msg);
    }

    public static void w(String tag, String ref, String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.w(tag, ref.isEmpty() ? msg : ref + " - " + msg);
        }
    }

    public static void w(String tag, String ref, String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            android.util.Log.w(tag, ref.isEmpty() ? msg : ref + " - " + msg, tr);
        }
    }
}
