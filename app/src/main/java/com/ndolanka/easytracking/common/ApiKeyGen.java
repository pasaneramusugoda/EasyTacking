package com.ndolanka.easytracking.common;

import android.content.Context;

import com.ndolanka.easytracking.R;

import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Pasan on 11/2/2016.
 */

public class ApiKeyGen {
    private static final String TAG = ApiKeyGen.class.getSimpleName();

    private String mAlgorithm;
    private String mEncKey;
    private String mTimeStamp;
    private String mKey;

    public ApiKeyGen(Context context) {
        mAlgorithm = context.getString(R.string.enc_algorithm);
        mEncKey = context.getString(R.string.enc_key);
    }

    private void GetApiKey() {
        mTimeStamp = Long.toString(System.currentTimeMillis());
        mKey = null;
        try {
            mKey = GetApiKey(mTimeStamp, mEncKey);
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {

            Log.e(TAG, e.getMessage());
        }
    }

    private String GetApiKey(String text, String keyString) throws
            UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {
        SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), mAlgorithm);
        Mac mac = Mac.getInstance(mAlgorithm);
        mac.init(key);

        byte[] bytes = mac.doFinal(text.getBytes("UTF-8"));

        return new String(Hex.encodeHex(bytes));
    }

    public void generate() {
        GetApiKey();
    }

    public String getKey() {
        return mKey;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }
}
