package com.ndolanka.easytracking.interfaces;

import android.view.View;

/**
 * Created by Pasan on 9/28/2016.
 */

public interface OnAdapterItemClickListener {
    void onItemClick(View view, int position, Object object);
}
