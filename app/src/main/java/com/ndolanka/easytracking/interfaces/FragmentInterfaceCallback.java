package com.ndolanka.easytracking.interfaces;

import android.view.View;

/**
 * Created by Pasan on 10/7/2016.
 */

public interface FragmentInterfaceCallback {
    public void onClick(View view, int identity, Object object);
}
