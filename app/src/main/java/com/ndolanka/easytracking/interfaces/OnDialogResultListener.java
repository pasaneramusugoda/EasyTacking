package com.ndolanka.easytracking.interfaces;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.Nullable;

/**
 * Created by Pasan on 11/7/2016.
 */

public interface OnDialogResultListener {
    void onPositiveResult(Dialog dialog, @Nullable Object value, @Nullable Class type);

    void onNegativeResult(Dialog dialog);

    void onDismiss(DialogInterface dialogInterface);
}
