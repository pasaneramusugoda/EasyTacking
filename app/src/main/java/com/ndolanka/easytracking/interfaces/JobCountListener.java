package com.ndolanka.easytracking.interfaces;

/**
 * Created by Pasan on 11/2/2016.
 */

public interface JobCountListener {
    public void onJobCountChange(int position, int count);
}
