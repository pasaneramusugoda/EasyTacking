package com.ndolanka.easytracking.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan on 11/2/2016.
 */

public class Job {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("job_no")
    @Expose
    private String jobNo;
    @SerializedName("order_reference")
    @Expose
    private String orderReference;
    @SerializedName("hbl_no")
    @Expose
    private String hblNo;
    @SerializedName("etd_date")
    @Expose
    private String etdDate;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("package_mode")
    @Expose
    private String packageMode;
    @SerializedName("no_of_pkgs")
    @Expose
    private String noOfPkgs;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("cbm")
    @Expose
    private String cbm;
    @SerializedName("closed")
    @Expose
    private String closed;
    @SerializedName("closed_on")
    @Expose
    private String closedOn;
    @SerializedName("current_status_id")
    @Expose
    private String currentStatusId;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("allocated_to_id")
    @Expose
    private String allocatedToId;
    @SerializedName("allocated_to")
    @Expose
    private String allocatedTo;
    @SerializedName("consignee")
    @Expose
    private String consignee;
    @SerializedName("consignee_code")
    @Expose
    private String consigneeCode;
    @SerializedName("milestones")
    @Expose
    private List<Milestone> milestones = new ArrayList<>();

    public Job() {
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return The jobNo
     */
    public String getJobNo() {
        return jobNo;
    }

    /**
     * @param jobNo The job_no
     */
    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    /**
     * @return The orderReference
     */
    public String getOrderReference() {
        return orderReference;
    }

    /**
     * @param orderReference The order_reference
     */
    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    /**
     * @return The hblNo
     */
    public String getHblNo() {
        return hblNo;
    }

    /**
     * @param hblNo The hbl_no
     */
    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    /**
     * @return The etdDate
     */
    public String getEtdDate() {
        return etdDate;
    }

    /**
     * @param etdDate The etd_date
     */
    public void setEtdDate(String etdDate) {
        this.etdDate = etdDate;
    }

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return The packageMode
     */
    public String getPackageMode() {
        return packageMode;
    }

    /**
     * @param packageMode The package_mode
     */
    public void setPackageMode(String packageMode) {
        this.packageMode = packageMode;
    }

    /**
     * @return The noOfPkgs
     */
    public String getNoOfPkgs() {
        return noOfPkgs;
    }

    /**
     * @param noOfPkgs The no_of_pkgs
     */
    public void setNoOfPkgs(String noOfPkgs) {
        this.noOfPkgs = noOfPkgs;
    }

    /**
     * @return The weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight The weight
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return The cbm
     */
    public String getCbm() {
        return cbm;
    }

    /**
     * @param cbm The cbm
     */
    public void setCbm(String cbm) {
        this.cbm = cbm;
    }

    /**
     * @return The closed
     */
    public String getClosed() {
        return closed;
    }

    /**
     * @param closed The closed
     */
    public void setClosed(String closed) {
        this.closed = closed;
    }

    /**
     * @return The closedOn
     */
    public String getClosedOn() {
        return closedOn;
    }

    /**
     * @param closedOn The closed_on
     */
    public void setClosedOn(String closedOn) {
        this.closedOn = closedOn;
    }

    /**
     * @return The currentStatusId
     */
    public String getCurrentStatusId() {
        return currentStatusId;
    }

    /**
     * @param currentStatusId The current_status_id
     */
    public void setCurrentStatusId(String currentStatusId) {
        this.currentStatusId = currentStatusId;
    }

    /**
     * @return The currentStatus
     */
    public String getCurrentStatus() {
        return currentStatus;
    }

    /**
     * @param currentStatus The current_status
     */
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAllocatedToId() {
        return allocatedToId;
    }

    public void setAllocatedToId(String allocatedToId) {
        this.allocatedToId = allocatedToId;
    }

    public String getAllocatedTo() {
        return allocatedTo;
    }

    public void setAllocatedTo(String allocatedTo) {
        this.allocatedTo = allocatedTo;
    }

    /**
     * @return The consignee
     */
    public String getConsignee() {
        return consignee;
    }

    /**
     * @param consignee The consignee
     */
    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    /**
     * @return The consigneeCode
     */
    public String getConsigneeCode() {
        return consigneeCode;
    }

    /**
     * @param consigneeCode The consignee_code
     */
    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    /**
     * @return The milestones
     */
    public List<Milestone> getMilestones() {
        return milestones;
    }

    /**
     * @param milestones The milestones
     */
    public void setMilestones(List<Milestone> milestones) {
        this.milestones = milestones;
    }
}