package com.ndolanka.easytracking.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan on 11/4/2016.
 */

public class Milestone {

    @SerializedName("milestone_id")
    @Expose
    private String milestoneId;
    @SerializedName("milestone")
    @Expose
    private String milestone;
    @SerializedName("completed")
    @Expose
    private String completed;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("completed_by_id")
    @Expose
    private String completedById;
    @SerializedName("completed_by")
    @Expose
    private String completedBy;
    @SerializedName("completed_on")
    @Expose
    private String completedOn;

    /**
     * @return The milestoneId
     */
    public String getMilestoneId() {
        return milestoneId;
    }

    /**
     * @param milestoneId The milestone_id
     */
    public void setMilestoneId(String milestoneId) {
        this.milestoneId = milestoneId;
    }

    /**
     * @return The milestone
     */
    public String getMilestone() {
        return milestone;
    }

    /**
     * @param milestone The milestone
     */
    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    /**
     * @return The completed
     */
    public String getCompleted() {
        return completed;
    }

    /**
     * @param completed The completed
     */
    public void setCompleted(String completed) {
        this.completed = completed;
    }

    /**
     * @return The remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks The remarks
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return The completedById
     */
    public String getCompletedById() {
        return completedById;
    }

    /**
     * @param completedById The completed_by_id
     */
    public void setCompletedById(String completedById) {
        this.completedById = completedById;
    }

    /**
     * @return The completedBy
     */
    public String getCompletedBy() {
        return completedBy;
    }

    /**
     * @param completedBy The completed_by
     */
    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    /**
     * @return The completedOn
     */
    public String getCompletedOn() {
        return completedOn;
    }

    /**
     * @param completedOn The completed_on
     */
    public void setCompletedOn(String completedOn) {
        this.completedOn = completedOn;
    }

}