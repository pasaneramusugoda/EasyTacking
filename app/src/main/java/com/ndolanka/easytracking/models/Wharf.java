package com.ndolanka.easytracking.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan on 11/16/2016.
 */

public class Wharf implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public Wharf() {
    }

    /**
     * @param id
     * @param name
     */
    public Wharf(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }


    protected Wharf(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Wharf> CREATOR = new Parcelable.Creator<Wharf>() {
        @Override
        public Wharf createFromParcel(Parcel in) {
            return new Wharf(in);
        }

        @Override
        public Wharf[] newArray(int size) {
            return new Wharf[size];
        }
    };
}