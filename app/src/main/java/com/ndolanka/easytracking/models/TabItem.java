package com.ndolanka.easytracking.models;

/**
 * Created by Pasan on 11/2/2016.
 */

public class TabItem {
    private String title;
    private Integer count;

    public TabItem(String title, Integer count) {
        this.title = title;
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
