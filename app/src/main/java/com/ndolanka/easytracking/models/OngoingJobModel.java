package com.ndolanka.easytracking.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan on 11/2/2016.
 */

public class OngoingJobModel {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("jobs")
    @Expose
    private List<Job> jobs = new ArrayList<Job>();

    /**
     * @return The result
     */
    public Boolean getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

    /**
     * @return The error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The jobs
     */
    public List<Job> getJobs() {
        return jobs;
    }

    /**
     * @param jobs The jobs
     */
    public void setJobs(List<Job> jobs) {
        this.jobs = jobs;
    }

}
