package com.ndolanka.easytracking.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Pasan on 11/4/2016.
 */

public class JobDetailModel {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("job")
    @Expose
    private Job job;

    /**
     * @return The result
     */
    public Boolean getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

    /**
     * @return The error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The job
     */
    public Job getJob() {
        return job;
    }

    /**
     * @param job The job
     */
    public void setJob(Job job) {
        this.job = job;
    }

}