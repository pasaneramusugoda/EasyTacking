package com.ndolanka.easytracking.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan on 11/16/2016.
 */

public class AllocateModel implements Parcelable {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("wharfs")
    @Expose
    private List<Wharf> wharfs = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     */
    public AllocateModel() {
    }

    /**
     * @param result
     * @param wharfs
     * @param error
     */
    public AllocateModel(Boolean result, String error, List<Wharf> wharfs) {
        this.result = result;
        this.error = error;
        this.wharfs = wharfs;
    }

    /**
     * @return The result
     */
    public Boolean getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Boolean result) {
        this.result = result;
    }

    /**
     * @return The error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The wharfs
     */
    public List<Wharf> getWharfs() {
        return wharfs;
    }

    /**
     * @param wharfs The wharfs
     */
    public void setWharfs(List<Wharf> wharfs) {
        this.wharfs = wharfs;
    }


    protected AllocateModel(Parcel in) {
        byte resultVal = in.readByte();
        result = resultVal == 0x02 ? null : resultVal != 0x00;
        error = in.readString();
        if (in.readByte() == 0x01) {
            wharfs = new ArrayList<Wharf>();
            in.readList(wharfs, Wharf.class.getClassLoader());
        } else {
            wharfs = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (result == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (result ? 0x01 : 0x00));
        }
        dest.writeString(error);
        if (wharfs == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(wharfs);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AllocateModel> CREATOR = new Parcelable.Creator<AllocateModel>() {
        @Override
        public AllocateModel createFromParcel(Parcel in) {
            return new AllocateModel(in);
        }

        @Override
        public AllocateModel[] newArray(int size) {
            return new AllocateModel[size];
        }
    };
}