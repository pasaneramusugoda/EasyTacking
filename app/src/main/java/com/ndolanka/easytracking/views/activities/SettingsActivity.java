package com.ndolanka.easytracking.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class SettingsActivity extends BaseActivity {

    public static final String TAG = SettingsActivity.class.getSimpleName();

    private Context mContext;
    private User mUser;
    private List<Call> mCalls = new ArrayList<>();

    @BindView(R.id.text_version)
    TextView mTextVersion;

    //region api

    private void logout() {
        startProgress();

        String deviceId = FirebaseInstanceId.getInstance().getToken();

        if (deviceId == null || deviceId.isEmpty()) {
            Toast.makeText(mContext, "Device id not received", Toast.LENGTH_SHORT).show();
            return;
        }

        mCalls.add(ApiUsage.Logout(mUser.getId(), mUser.getUserType(), deviceId, new ApiCallback<ResponseBody>() {
            @Override
            public void success(int code, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, "Logout", jsonString);
                    JSONObject object = new JSONObject(jsonString);

                    if (object.getBoolean("result")) {
                        mUser.setLogged(false);
                        mUser.setDeviceId(null);
                        Preference.getInstance(mContext).setCurrentUser(mUser);
                        finish();
                    } else {
                        Toast.makeText(mContext, object.getString("error"), Toast.LENGTH_LONG).show();
                    }
                } catch (IOException | JSONException e) {
                    FirebaseCrash.report(e);
                    Log.e(TAG, "Logout", e.getMessage());
                    Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "Logout", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
            }
        }));
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mContext = this;
        mUser = Preference.getInstance(mContext).getCurrentUser();

        PackageInfo pInfo = null;
        try {
            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pInfo != null) {
            mTextVersion.setText(String.format("v%s", pInfo.versionName));
        } else {
            mTextVersion.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStop() {
        cancelRequests(mCalls);
        super.onStop();
    }

    @OnClick({R.id.text_change_password, R.id.text_logout, R.id.button_about})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_change_password:
                Intent intent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
                startActivityForResult(intent, ChangePasswordActivity.REQUEST_CODE);
                break;
            case R.id.text_logout:
                logout();
                break;
            case R.id.button_about:
                startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChangePasswordActivity.REQUEST_CODE && resultCode == RESULT_OK) {
            logout();
        }
    }
}
