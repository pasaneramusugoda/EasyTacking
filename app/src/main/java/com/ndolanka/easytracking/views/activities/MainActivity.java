package com.ndolanka.easytracking.views.activities;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.adapters.MainPagerAdapter;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.interfaces.JobCountListener;
import com.ndolanka.easytracking.models.TabItem;
import com.ndolanka.easytracking.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements JobCountListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_NEW_ALLOCATION = "new_allocation";
    private Context mContext;
    private User mUser;
    private List<TabItem> mTabs = new ArrayList<>();

    private MainPagerAdapter mPagerAdapter;

    //region ui

    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    //endregion

    //region imp

    private void setupTabLayout() {
        mTabs.add(new TabItem("ONGOING", 0));
        mTabs.add(new TabItem("COM. TODAY", 0));
        mTabs.add(new TabItem("COMPLETED", 0));

        mPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), mContext, mTabs);
        mPagerAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    TabLayout.Tab tab = mTabLayout.getTabAt(i);
                    if (tab != null)
                        tab.setCustomView(mPagerAdapter.getTabView(i));
                }
            }
        });
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null)
                tab.setCustomView(mPagerAdapter.getTabView(i));
        }
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addContentView(
                getLayoutInflater().inflate(R.layout.layout_progress, null),
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        ButterKnife.bind(this);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        setupTabLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_allocate);
        mUser = Preference.getInstance(mContext).getCurrentUser();

        if (item != null && (mUser.getUserType().equals("A") || mUser.getUserType().equals("T")))
            item.setVisible(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            baseStartActivity(MainActivity.this, SettingsActivity.class);
            return true;
        } else if (id == R.id.action_allocate) {
            baseStartActivity(MainActivity.this, AllocateActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onJobCountChange(final int position, final int count) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mTabs.get(position).setCount(count);
                mPagerAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUser = Preference.getInstance(mContext).getCurrentUser();
        if (mUser == null || !mUser.getLogged()) {
            baseStartActivity(MainActivity.this, LoginActivity.class, ActivityFinish.ONE);
        }
    }
}
