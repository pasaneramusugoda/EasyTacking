package com.ndolanka.easytracking.views.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.ndolanka.easytracking.R;

/**
 * Created by Pasan Eramusugoda on 7/8/2016.
 */

public class TextView extends AppCompatTextView {
    public TextView(Context context) {
        super(context);

        if (this.isInEditMode())
            return;

        setFont(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (this.isInEditMode())
            return;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.View, 0, 0);
        try {
            setFont(context, ta.getInt(R.styleable.View_fontType, 0), ta.getBoolean(R.styleable.View_setOnlyDefaultFont, false));
        } finally {
            ta.recycle();
        }
    }

    public TextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (this.isInEditMode())
            return;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.View, defStyleAttr, 0);
        try {
            setFont(context, ta.getInt(R.styleable.View_fontType, 0), ta.getBoolean(R.styleable.View_setOnlyDefaultFont, false));
        } finally {
            ta.recycle();
        }
    }

    //region font

    private void setFont(Context context) {
        setFont(context, 0, true);
    }

    private void setFont(Context context, int i, boolean setOnlyDefaultFont) {
        //        Typeface typeface = FontCache.get(Functions.GetFontPath(i), context);
        //        setTypeface(typeface);
    }

    //endregion
}
