package com.ndolanka.easytracking.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

    private static final String TAG = ChangePasswordActivity.class.getSimpleName();
    public static final int REQUEST_CODE = 906;

    private Context mContext;
    private User mUser;
    private List<Call> mCalls = new ArrayList<>();

    //region ui
    @BindView(R.id.edit_text_password)
    EditText mEditTextPassword;
    @BindView(R.id.edit_text_n_password)
    EditText mEditTextNPassword;
    @BindView(R.id.edit_text_c_password)
    EditText mEditTextCPassword;
    //endregion

    //region api

    private void finishThis() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    private void submit() {
        startProgress();
        mCalls.add(ApiUsage.ChangePassword(mUser.getUsername(), mEditTextPassword.getText().toString(), mEditTextCPassword.getText().toString(),
                new ApiCallback<ResponseBody>() {
                    @Override
                    public void success(int code, Response<ResponseBody> response) {
                        try {
                            String jsonString = response.body().string();
                            Log.d(TAG, "ChangePassword", jsonString);
                            JSONObject object = new JSONObject(jsonString);

                            if (object.getBoolean("result")) {
                                Toast.makeText(mContext, "Password has been changed successfully", Toast.LENGTH_LONG).show();
                                finishThis();
                            } else {
                                Toast.makeText(mContext, object.getString("error"), Toast.LENGTH_LONG).show();
                            }
                        } catch (IOException | JSONException e) {
                            FirebaseCrash.report(e);
                            Log.e(TAG, "ChangePassword", e.getMessage());
                            Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void failed(int code, Response<?> response, Throwable t) {
                        String msg = "Code: " + String.valueOf(code);
                        if (response != null) {
                            msg += " Response: " + response.message();
                        } else if (t != null) {
                            msg += " Error: " + t.getMessage();
                        }
                        Log.e(TAG, "ChangePassword", msg);
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                        if (t != null)
                            FirebaseCrash.report(t);

                        FirebaseCrash.log(msg);
                    }

                    @Override
                    public void atLast(Call<?> call) {
                        stopProgress();
                    }
                }));
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        addContentView(
                getLayoutInflater().inflate(R.layout.layout_progress, null),
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        mContext = this;
        mUser = Preference.getInstance(mContext).getCurrentUser();

        mEditTextPassword.setTextInputLayout(R.id.input_layout_password);
        mEditTextPassword.addValidator(Functions.ValidationRequired(mContext));

        mEditTextNPassword.setTextInputLayout(R.id.input_layout_n_password);
        mEditTextNPassword.addValidator(Functions.ValidationRequired(mContext));

        mEditTextCPassword.setTextInputLayout(R.id.input_layout_c_password);
        mEditTextCPassword.addValidator(Functions.ValidationRequired(mContext));
    }

    @OnClick(R.id.button_submit)
    public void onClick(View view) {
        if (Functions.ValidateFields(mEditTextPassword, mEditTextNPassword, mEditTextCPassword)) {
            if (Functions.IsValidPassword(mEditTextNPassword.getText().toString(), mEditTextCPassword.getText().toString())) {
                submit();
            } else {
                Toast.makeText(mContext, "Passwords are not matching", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
