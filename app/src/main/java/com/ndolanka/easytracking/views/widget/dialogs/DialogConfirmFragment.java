package com.ndolanka.easytracking.views.widget.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.interfaces.OnDialogResultListener;
import com.ndolanka.easytracking.views.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogConfirmFragment extends AppCompatDialogFragment {
    public static final String TAG = DialogConfirmFragment.class.getSimpleName();
    private static final String ARG_PARAM_TITLE = "paramTitle";
    private static final String ARG_PARAM_MESSAGE = "paramMessage";

    private Context mContext;
    private String mParamTitle;
    private String mParamMessage;
    private OnDialogResultListener mListener;

    //region ui

    @BindView(R.id.text_title)
    TextView mT_Title;
    @BindView(R.id.text_message)
    TextView mT_Message;

    //endregion

    //region imp

    private void setResultAndClose(int action) {
        if (mListener == null) return;

        switch (action) {
            case R.id.button_yes:
                mListener.onPositiveResult(this.getDialog(), null, null);
                break;
            case R.id.button_no:
                mListener.onNegativeResult(this.getDialog());
                break;
        }
    }

    //endregion

    public DialogConfirmFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param paramTitle   Parameter 1.
     * @param paramMessage Parameter 2.
     * @return A new instance of fragment DialogConfirmFragment.
     */
    public static DialogConfirmFragment newInstance(String paramTitle, String paramMessage) {
        DialogConfirmFragment fragment = new DialogConfirmFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, paramTitle);
        args.putString(ARG_PARAM_MESSAGE, paramMessage);
        fragment.setArguments(args);
        return fragment;
    }

    public static DialogConfirmFragment newInstance(String paramMessage) {
        DialogConfirmFragment fragment = new DialogConfirmFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, null);
        args.putString(ARG_PARAM_MESSAGE, paramMessage);
        fragment.setArguments(args);
        return fragment;
    }

    public void setDialogResultListener(OnDialogResultListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParamTitle = getArguments().getString(ARG_PARAM_TITLE);
            mParamMessage = getArguments().getString(ARG_PARAM_MESSAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_confirm, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (mParamTitle == null || mParamTitle.isEmpty()) {
            mT_Title.setVisibility(View.GONE);
        } else {
            mT_Title.setText(mParamTitle);
        }

        mT_Message.setText(mParamMessage);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        final RelativeLayout root = new RelativeLayout(mContext);
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @OnClick({R.id.button_yes, R.id.button_no})
    public void onClick(View view) {
        setResultAndClose(view.getId());
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mListener != null)
            mListener.onDismiss(dialog);
        super.onDismiss(dialog);
    }
}
