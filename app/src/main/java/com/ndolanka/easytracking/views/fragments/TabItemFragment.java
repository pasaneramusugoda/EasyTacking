package com.ndolanka.easytracking.views.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.adapters.JobAdapter;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.interfaces.EndlessRecyclerViewScrollListener;
import com.ndolanka.easytracking.interfaces.JobCountListener;
import com.ndolanka.easytracking.interfaces.OnAdapterItemClickListener;
import com.ndolanka.easytracking.models.Job;
import com.ndolanka.easytracking.models.OngoingJobModel;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.activities.JobDetailActivity;
import com.ndolanka.easytracking.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TabItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabItemFragment extends Fragment implements OnAdapterItemClickListener, SearchView.OnQueryTextListener {
    private static final String TAG = TabItemFragment.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";

    private Context mContext;
    private User mUser;
    private List<Call> mCalls = new ArrayList<>();
    private Integer mPosition;
    private List<Job> mJobs = new ArrayList<>();
    private JobAdapter mAdapter;
    private JobCountListener mJobCountListener;
    private Integer mLimit = 5;

    //region ui

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;
    @BindView(R.id.rv_jobs)
    RecyclerView mRVJobs;

    //endregion

    //region api

    private void getJobs(Integer position, int page) {
        getJobs(false, position, page);
    }

    private void getJobs(final boolean swipe, Integer position, int page) {

        if (page == 0) {
            mJobs.clear();

            if (swipe)
                mSwipeContainer.setRefreshing(true);
            else
                getParentAct().startProgress();
        }

        switch (position) {
            case 1:
                //region ongoing
                mCalls.add(ApiUsage.GetOngoingJobs(mUser.getId(), mUser.getUserType(), new ApiCallback<OngoingJobModel>() {
                    @Override
                    public void success(int code, Response<OngoingJobModel> response) {
                        OngoingJobModel model = response.body();
                        Log.d(TAG, "GetOngoingJobs", new Gson().toJson(model));

                        if (model.getResult()) {
                            if (model.getJobs() != null && !model.getJobs().isEmpty()) {
                                mJobs.clear();
                                mJobs.addAll(model.getJobs());
                            }
                        } else {
                            Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void failed(int code, Response<?> response, Throwable t) {
                        String msg = "Code: " + String.valueOf(code);
                        if (response != null) {
                            msg += " Response: " + response.message();
                        } else if (t != null) {
                            msg += " Error: " + t.getMessage();
                        }
                        Log.e(TAG, "GetOngoingJobs", msg);
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                        if (t != null)
                            FirebaseCrash.report(t);

                        FirebaseCrash.log(msg);
                    }

                    @Override
                    public void atLast(Call<?> call) {
                        if (swipe)
                            mSwipeContainer.setRefreshing(false);
                        else
                            getParentAct().stopProgress();

                        if (mJobCountListener != null)
                            mJobCountListener.onJobCountChange(mPosition - 1, mJobs.size());

                        if (!mJobs.isEmpty())
                            mAdapter.notifyDataSetChanged();
                    }
                }));
                //endregion
                break;
            case 2:
                //region today
                mCalls.add(ApiUsage.GetTodayCompletedJobs(mUser.getId(), mUser.getUserType(), new ApiCallback<OngoingJobModel>() {
                    @Override
                    public void success(int code, Response<OngoingJobModel> response) {
                        OngoingJobModel model = response.body();
                        Log.d(TAG, "GetTodayCompletedJobs", new Gson().toJson(model));

                        if (model.getResult()) {
                            if (model.getJobs() != null && !model.getJobs().isEmpty()) {
                                mJobs.clear();
                                mJobs.addAll(model.getJobs());
                            }
                        } else {
                            Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void failed(int code, Response<?> response, Throwable t) {
                        String msg = "Code: " + String.valueOf(code);
                        if (response != null) {
                            msg += " Response: " + response.message();
                        } else if (t != null) {
                            msg += " Error: " + t.getMessage();
                        }
                        Log.e(TAG, "GetTodayCompletedJobs", msg);
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                        if (t != null)
                            FirebaseCrash.report(t);

                        FirebaseCrash.log(msg);
                    }

                    @Override
                    public void atLast(Call<?> call) {
                        if (swipe)
                            mSwipeContainer.setRefreshing(false);
                        else
                            getParentAct().stopProgress();

                        if (mJobCountListener != null)
                            mJobCountListener.onJobCountChange(mPosition - 1, mJobs.size());

                        if (!mJobs.isEmpty())
                            mAdapter.notifyDataSetChanged();
                    }
                }));
                //endregion
                break;
            case 3:
                //region completed
                mCalls.add(ApiUsage.GetAllCompletedJobs(mUser.getId(), mUser.getUserType(), String.valueOf(page * mLimit), String.valueOf(mLimit), new ApiCallback<OngoingJobModel>() {
                    @Override
                    public void success(int code, Response<OngoingJobModel> response) {
                        OngoingJobModel model = response.body();
                        Log.d(TAG, "GetAllCompletedJobs", new Gson().toJson(model));

                        if (model.getResult()) {
                            if (model.getJobs() != null && !model.getJobs().isEmpty()) {
                                mJobs.addAll(model.getJobs());
                            }
                        } else {
                            Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void failed(int code, Response<?> response, Throwable t) {
                        String msg = "Code: " + String.valueOf(code);
                        if (response != null) {
                            msg += " Response: " + response.message();
                        } else if (t != null) {
                            msg += " Error: " + t.getMessage();
                        }
                        Log.e(TAG, "GetAllCompletedJobs", msg);
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                        if (t != null)
                            FirebaseCrash.report(t);

                        FirebaseCrash.log(msg);
                    }

                    @Override
                    public void atLast(Call<?> call) {
                        if (swipe)
                            mSwipeContainer.setRefreshing(false);
                        else
                            getParentAct().stopProgress();

                        if (!mJobs.isEmpty())
                            mAdapter.notifyDataSetChanged();
                    }
                }));
                //endregion
                break;
        }
    }

    //endregion

    //region imp
    private MainActivity getParentAct() {
        return (MainActivity) mContext;
    }

    private List<Job> filter(List<Job> models, String query) {
        query = query.toLowerCase();
        final List<Job> filteredModelList = new ArrayList<>();
        for (Job model : models) {
            final String text = model.getJobNo().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
    //endregion

    public TabItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @return A new instance of fragment TabItemFragment.
     */
    public static TabItemFragment newInstance(Integer position) {
        TabItemFragment fragment = new TabItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPosition = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRVJobs.setLayoutManager(layoutManager);
        mRVJobs.setNestedScrollingEnabled(true);
        mRVJobs.setHasFixedSize(true);
        if (mPosition == 2) {
            mRVJobs.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    getJobs(mPosition, page);
                }
            });
        }

        mAdapter = new JobAdapter(mContext, mJobs, this);
        mRVJobs.setAdapter(mAdapter);

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJobs(true, mPosition, 0);
            }
        });

        getJobs(mPosition, 0);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof JobCountListener) {
            mJobCountListener = (JobCountListener) context;
        } else {
            throw new IllegalArgumentException("Must implement JobCountListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mAdapter.setFilter(mJobs);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Job> filteredModelList = filter(mJobs, newText);

        mAdapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public void onItemClick(View view, int position, Object object) {
        Intent intent = new Intent(getActivity(), JobDetailActivity.class);
        intent.putExtra(JobDetailActivity.EXTRA_JOB_ID, ((Job) object).getId());
        startActivity(intent);
    }
}
