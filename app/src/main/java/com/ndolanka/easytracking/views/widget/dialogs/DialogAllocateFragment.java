package com.ndolanka.easytracking.views.widget.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.interfaces.OnDialogResultListener;
import com.ndolanka.easytracking.models.Wharf;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogAllocateFragment extends AppCompatDialogFragment {
    public static final String TAG = DialogAllocateFragment.class.getSimpleName();
    private static final String ARG_PARAM_SELECTED_INDEX = "param_selected_index";
    private static final String ARG_PARAM_ITEMS = "param_items";

    private Context mContext;
    private int mSelectedIndex;
    private List<Wharf> mItems = new ArrayList<>();
    private OnDialogResultListener mListener;

    @BindView(R.id.rg)
    RadioGroup mRG;

    public DialogAllocateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DialogRemarkFragment.
     */
    public static DialogAllocateFragment newInstance(int selectedIndex, List<Wharf> items) {
        DialogAllocateFragment fragment = new DialogAllocateFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_SELECTED_INDEX, selectedIndex);
        args.putParcelableArrayList(ARG_PARAM_ITEMS, (ArrayList<? extends Parcelable>) items);
        fragment.setArguments(args);
        return fragment;
    }

    public void setDialogResultListener(OnDialogResultListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSelectedIndex = getArguments().getInt(ARG_PARAM_SELECTED_INDEX, -1);
            mItems = getArguments().getParcelableArrayList(ARG_PARAM_ITEMS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_allocate, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        int index = 1;
        for (Wharf item : mItems) {
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(
                    getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0,
                    getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin), 0);

            RadioButton button = new RadioButton(mContext);
            button.setLayoutParams(params);
            button.setPadding(
                    getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin),
                    getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin),
                    getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin),
                    getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin));

            button.setId(Integer.parseInt(item.getId()));
            button.setText(item.getName());
            button.setChecked(mSelectedIndex == Integer.parseInt(item.getId()));

            if (index % 2 == 0) {
                button.setBackgroundColor(Functions.GetColor(mContext, R.color.colorDivider2));
            }

            mRG.addView(button);
            index++;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        final RelativeLayout root = new RelativeLayout(mContext);
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.button_cancel, R.id.button_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_cancel:
                if (mListener != null) {
                    mListener.onNegativeResult(getDialog());
                }
                break;
            case R.id.button_submit:
                if (mListener != null) {
                    mListener.onPositiveResult(getDialog(), mRG.getCheckedRadioButtonId(), Integer.class);
                }
                break;
        }
    }
}
