package com.ndolanka.easytracking.views.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Transition;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Pasan on 11/1/2016.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    private int mAnimDuration = 500;
    private int mProgressCount = 0;

    private View mProgress;

    public void startProgress() {
        if (mProgress != null) {
            mProgressCount++;

            if (mProgressCount == 1) {
                mProgress.setAlpha(0f);
                mProgress.setVisibility(View.VISIBLE);
                ObjectAnimator
                        .ofFloat(mProgress, View.ALPHA, 0, 1)
                        .setDuration(mAnimDuration)
                        .start();
            }
        }
    }

    public void stopProgress() {
        if (mProgress != null && mProgress.getVisibility() == View.VISIBLE) {
            if (mProgressCount - 1 >= 0) {
                mProgressCount--;
            }

            if (mProgressCount == 0) {
                ObjectAnimator animator = ObjectAnimator
                        .ofFloat(mProgress, View.ALPHA, 1, 0)
                        .setDuration(mAnimDuration);
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        mProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {
                        mProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                animator.start();
            }
        }
    }

    private void setupProgress() {
        mProgress = findViewById(R.id.progressBarContainer);
        mProgress.setAlpha(0f);
        mProgress.setVisibility(View.GONE);
    }

    public void baseStartActivity(Activity activity, Class activityCall) {
        baseStartActivity(activity, activityCall, ActivityFinish.NONE);
    }

    public void baseStartActivity(Activity activity, Class activityCall, ActivityFinish finish) {
        Intent intent = new Intent(activity, activityCall);

        if (finish == ActivityFinish.ALL)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);

        if (finish == ActivityFinish.ONE) {
            finish();
        }
    }

    private void setupWindowAnimations() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Transition enterTrans = new Fade();
            getWindow().setEnterTransition(enterTrans);

            Transition exitTrans = new Fade();
            getWindow().setExitTransition(exitTrans);
        }
    }

    private void writeStatus(boolean status) {
        Preference.getInstance(getApplicationContext()).write(Preference.Keys.TOKEN_REFRESH_NEEDED, status);
    }

    private void checkDeviceToken() {
        User user = Preference.getInstance(this).getCurrentUser();

        if (user == null || !user.getLogged())
            return;

        boolean status = Preference.getInstance(this).get(Preference.Keys.TOKEN_REFRESH_NEEDED, true);
        if (status) {
            String token = FirebaseInstanceId.getInstance().getToken();
            ApiUsage.UpdateDeviceId(user.getId(), user.getUserType(), user.getDeviceId(), token, new ApiCallback<ResponseBody>() {
                @Override
                public void success(int code, Response<ResponseBody> response) {
                    try {
                        String jsonString = response.body().string();
                        Log.d(TAG, "UpdateDeviceId", jsonString);
                        JSONObject object = new JSONObject(jsonString);

                        if (object.getBoolean("result")) {
                            Log.d(TAG, "UpdateDeviceId", "success");
                            writeStatus(false);
                        } else {
                            writeStatus(true);
                        }
                    } catch (IOException | JSONException e) {
                        FirebaseCrash.report(e);
                        Log.e(TAG, "UpdateDeviceId", e.getMessage());
                        writeStatus(true);
                    }
                }

                @Override
                public void failed(int code, Response<?> response, Throwable t) {
                    String msg = "Code: " + String.valueOf(code);
                    if (response != null) {
                        msg += " Response: " + response.message();
                    } else if (t != null) {
                        msg += " Error: " + t.getMessage();
                    }

                    Log.e(TAG, "UpdateDeviceId", msg, t);

                    writeStatus(true);
                }

                @Override
                public void atLast(Call<?> call) {

                }
            });
        }
    }

    public void cancelRequests(List<Call> calls) {
        if (calls == null)
            return;

        for (Call call : calls) {
            if (!call.isCanceled()) {
                try {
                    call.cancel();
                    Log.d(TAG, "cancelRequests", call.request().url().toString());
                } catch (Exception ex) {
                    Log.e(TAG, "cancelRequests", ex.getMessage(), ex);
                }
            }
        }
    }

    public void cancelRequests(Call call) {
        if (call == null)
            return;

        if (!call.isCanceled()) {
            try {
                call.cancel();
                Log.d(TAG, "cancelRequests", call.request().url().toString());
            } catch (Exception ex) {
                Log.e(TAG, "cancelRequests", ex.getMessage(), ex);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                checkDeviceToken();
            }
        });
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        setupWindowAnimations();
    }

    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params) {
        super.addContentView(view, params);
        setupProgress();
    }

    public enum ActivityFinish {
        NONE, ONE, ALL
    }
}
