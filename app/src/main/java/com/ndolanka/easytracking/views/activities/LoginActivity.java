package com.ndolanka.easytracking.views.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.models.UserModel;
import com.ndolanka.easytracking.views.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private Context mContext;
    private User mUser;

    //region ui
    @BindView(R.id.edit_text_username)
    EditText mUsername;
    @BindView(R.id.edit_text_password)
    EditText mPassword;
    //endregion

    //region api

    private void login() {
        final String deviceId = FirebaseInstanceId.getInstance().getToken();

        if (deviceId == null || deviceId.isEmpty()) {
            Toast.makeText(mContext, "Device id not received yet. Please try again.", Toast.LENGTH_LONG).show();
            return;
        } else {
            Log.d(TAG, "Device Id", deviceId);
        }

        startProgress();
        ApiUsage.Login(deviceId, mUsername.getText().toString(), mPassword.getText().toString(), new ApiCallback<UserModel>() {
            @Override
            public void success(int code, Response<UserModel> response) {
                UserModel model = response.body();
                Log.d(TAG, "Login", new Gson().toJson(model));

                if (model.getResult()) {
                    if (model.getUser() != null) {
                        mUser = model.getUser();
                        mUser.setLogged(true);
                        mUser.setDeviceId(deviceId);
                        Preference.getInstance(mContext).setCurrentUser(mUser);
                        baseStartActivity(LoginActivity.this, MainActivity.class, ActivityFinish.ONE);
                    } else {
                        Toast.makeText(mContext, "Some data not received.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "Login", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
            }
        });
    }

    //endregion

    //region imp

    private void init() {
        mUsername.setTextInputLayout(R.id.input_layout_username);
        mUsername.addValidator(Functions.ValidationRequired(mContext));

        mPassword.setTextInputLayout(R.id.input_layout_password);
        mPassword.addValidator(Functions.ValidationRequired(mContext));

        if (mUser != null)
            mUsername.setText(mUser.getUsername());
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addContentView(
                getLayoutInflater().inflate(R.layout.layout_progress, null),
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        ButterKnife.bind(this);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        try {
            if (mUser != null && mUser.getLogged())
                baseStartActivity(LoginActivity.this, MainActivity.class, ActivityFinish.ONE);
        } catch (NullPointerException ignored) {
        }

        init();
    }

    @OnClick({R.id.button_login})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                if (Functions.ValidateFields(mUsername, mPassword)) {
                    login();
                }
                break;
        }
    }
}
