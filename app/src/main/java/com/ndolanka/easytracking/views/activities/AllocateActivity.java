package com.ndolanka.easytracking.views.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.adapters.JobAllocateAdapter;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.interfaces.OnAdapterItemClickListener;
import com.ndolanka.easytracking.interfaces.OnDialogResultListener;
import com.ndolanka.easytracking.models.AllocateModel;
import com.ndolanka.easytracking.models.Job;
import com.ndolanka.easytracking.models.OngoingJobModel;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.models.Wharf;
import com.ndolanka.easytracking.views.widget.dialogs.DialogAllocateFragment;
import com.ndolanka.easytracking.views.widget.dialogs.DialogConfirmFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AllocateActivity extends BaseActivity implements OnAdapterItemClickListener, SearchView.OnQueryTextListener {

    private static final String TAG = AllocateActivity.class.getSimpleName();
    public static final String EXTRA_JOB_ID = "job_id";

    private Context mContext;
    private User mUser;
    private List<Call> mCalls = new ArrayList<>();
    private List<Job> mJobs = new ArrayList<>();
    private List<Wharf> mWharfs = new ArrayList<>();
    private JobAllocateAdapter mAdapter;

    //region ui

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeContainer;
    @BindView(R.id.rv_jobs)
    RecyclerView mRVJobs;

    //endregion

    //region api

    private void getJobs() {
        getJobs(false);
    }

    private void getJobs(final boolean swipe) {

        if (swipe)
            mSwipeContainer.setRefreshing(true);
        else
            startProgress();

        mCalls.add(ApiUsage.GetJobsToAllocate(new ApiCallback<OngoingJobModel>() {
            @Override
            public void success(int code, Response<OngoingJobModel> response) {
                OngoingJobModel model = response.body();
                Log.d(TAG, "GetJobsToAllocate", new Gson().toJson(model));

                if (model.getResult()) {
                    if (model.getJobs() != null && !model.getJobs().isEmpty()) {
                        mJobs.clear();
                        mJobs.addAll(model.getJobs());
                    }
                } else {
                    Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "GetJobsToAllocate", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                if (swipe)
                    mSwipeContainer.setRefreshing(false);
                else
                    stopProgress();

                if (!mJobs.isEmpty())
                    mAdapter.notifyDataSetChanged();
            }
        }));
    }

    private void getWharfList(final String jobId, final int id) {
        startProgress();
        mCalls.add(ApiUsage.GetWharfList(new ApiCallback<AllocateModel>() {
            @Override
            public void success(int code, Response<AllocateModel> response) {
                AllocateModel model = response.body();
                Log.d(TAG, "GetWharfList", new Gson().toJson(model));

                if (model.getResult()) {
                    if (model.getWharfs() != null && !model.getWharfs().isEmpty()) {
                        mWharfs.clear();
                        mWharfs.addAll(model.getWharfs());
                    }
                } else {
                    Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "GetWharfList", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
                if (!mWharfs.isEmpty()) {
                    showAllocateDialog(jobId, id, mWharfs);
                } else {
                    Toast.makeText(mContext, "Sorry no wharfs found.", Toast.LENGTH_SHORT).show();
                }
            }
        }));
    }

    private void allocateJob(String jobId, String allocationId, String oldAllocatedId) {
        startProgress();
        mCalls.add(ApiUsage.AllocateJob(mUser.getId(), jobId, allocationId, oldAllocatedId, new ApiCallback<ResponseBody>() {
            @Override
            public void success(int code, Response<ResponseBody> response) {
                try {
                    String jsonString = response.body().string();
                    Log.d(TAG, "AllocateJob", jsonString);
                    JSONObject object = new JSONObject(jsonString);

                    if (object.getBoolean("result")) {
                        Log.d(TAG, "AllocateJob", "success");
                        Toast.makeText(mContext, "Successfully allocated.", Toast.LENGTH_SHORT).show();
                        getJobs();
                    } else {
                        Toast.makeText(mContext, object.getString("error"), Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    FirebaseCrash.report(e);
                    Log.e(TAG, "AllocateJob", e.getMessage());
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }

                Log.e(TAG, "AllocateJob", msg, t);
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
            }
        }));
    }

    //endregion

    //region imp
    private List<Job> filter(List<Job> models, String query) {
        query = query.toLowerCase();
        final List<Job> filteredModelList = new ArrayList<>();
        for (Job model : models) {
            final String text = model.getJobNo().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private void showAllocationConfirm(final String jobId, final int preId, final int id) {
        if (preId == id)
            return;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogConfirmFragment newFragment = DialogConfirmFragment.newInstance(
                getString(R.string.dialog_title_confirm), getString(R.string.dialog_message_confirm));
        newFragment.setDialogResultListener(new OnDialogResultListener() {
            @Override
            public void onPositiveResult(Dialog dialog, @Nullable Object value, @Nullable Class type) {
                allocateJob(jobId, String.valueOf(id), String.valueOf(preId));
                dialog.dismiss();
            }

            @Override
            public void onNegativeResult(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
        newFragment.show(ft, DialogConfirmFragment.TAG);
    }

    private void showAllocateDialog(final String jobId, final int id, List<Wharf> items) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogAllocateFragment newFragment = DialogAllocateFragment.newInstance(id, items);
        newFragment.setDialogResultListener(new OnDialogResultListener() {
            @Override
            public void onPositiveResult(Dialog dialog, @Nullable Object value, @Nullable Class type) {
                Log.d(TAG, "showAllocateDialog", String.valueOf(value));
                dialog.dismiss();
                showAllocationConfirm(jobId, id, (Integer) value);
            }

            @Override
            public void onNegativeResult(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
        newFragment.show(ft, DialogAllocateFragment.TAG);
    }
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allocate);
        addContentView(
                getLayoutInflater().inflate(R.layout.layout_progress, null),
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRVJobs.setLayoutManager(layoutManager);
        mRVJobs.setHasFixedSize(true);

        mAdapter = new JobAllocateAdapter(mContext, mJobs, this);
        mRVJobs.setAdapter(mAdapter);

        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJobs(true);
            }
        });

        getJobs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_allocate, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        mAdapter.setFilter(mJobs);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Job> filteredModelList = filter(mJobs, newText);
        mAdapter.setFilter(filteredModelList);
        return true;
    }

    @Override
    public void onItemClick(View view, int position, Object object) {
        if (view.getId() == R.id.view_allocated) {
            Job job = (Job) object;
            int id = job.getAllocatedToId() == null ? -1 : Integer.parseInt(job.getAllocatedToId());
            if (mWharfs.isEmpty())
                getWharfList(job.getId(), id);
            else
                showAllocateDialog(job.getId(), id, mWharfs);
        }
    }
}
