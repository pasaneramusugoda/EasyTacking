package com.ndolanka.easytracking.views.activities;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.adapters.MilestoneAdapter;
import com.ndolanka.easytracking.api.ApiCallback;
import com.ndolanka.easytracking.api.ApiUsage;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.common.Log;
import com.ndolanka.easytracking.common.Preference;
import com.ndolanka.easytracking.interfaces.OnAdapterItemClickListener;
import com.ndolanka.easytracking.interfaces.OnDialogResultListener;
import com.ndolanka.easytracking.models.Job;
import com.ndolanka.easytracking.models.JobDetailModel;
import com.ndolanka.easytracking.models.Milestone;
import com.ndolanka.easytracking.models.User;
import com.ndolanka.easytracking.views.widget.TextView;
import com.ndolanka.easytracking.views.widget.dialogs.DialogConfirmFragment;
import com.ndolanka.easytracking.views.widget.dialogs.DialogRemarkFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;

public class JobDetailActivity extends BaseActivity implements OnAdapterItemClickListener {

    private static final String TAG = JobDetailActivity.class.getSimpleName();
    public static final String EXTRA_JOB_ID = "job_id";

    private Context mContext;
    private User mUser;
    private List<Call> mCalls = new ArrayList<>();
    private Job mJob;
    private List<Milestone> mMilestones = new ArrayList<>();
    private MilestoneAdapter mAdapter;

    private String mJobId;

    //region ui

    @BindView(R.id.activity_job_detail)
    View mVParent;
    @BindView(R.id.text_client_name)
    TextView mTClientName;
    @BindView(R.id.text_description)
    TextView mTDescription;
    @BindView(R.id.text_hbl_number)
    TextView mTHblNumber;
    @BindView(R.id.text_order_ref_number)
    TextView mTOrderRefNumber;
    @BindView(R.id.text_status)
    TextView mTStatus;
    @BindView(R.id.rv_milestones)
    RecyclerView mRVMilestones;

    //endregion

    //region api

    private void getJobDetail() {
        startProgress();
        mCalls.add(ApiUsage.GetJobDetail(mJobId, mUser.getUserType(), new ApiCallback<JobDetailModel>() {
            @Override
            public void success(int code, Response<JobDetailModel> response) {
                JobDetailModel model = response.body();
                Log.d(TAG, "GetJobDetail", new Gson().toJson(model));

                if (model.getResult()) {
                    if (model.getJob() != null) {
                        mJob = model.getJob();
                    } else {
                        Toast.makeText(mContext, "Some data not received.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "GetJobDetail", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
                if (mJob != null)
                    init();
            }
        }));
    }

    private void updateJobStatus(String milestoneId, String completed, String remarks) {
        startProgress();
        mCalls.add(ApiUsage.UpdateJobStatus(mUser.getId(), mJobId, milestoneId, completed, remarks, new ApiCallback<JobDetailModel>() {
            @Override
            public void success(int code, Response<JobDetailModel> response) {
                JobDetailModel model = response.body();
                Log.d(TAG, "UpdateJobStatus", new Gson().toJson(model));

                if (model.getResult()) {
                    if (model.getJob() != null) {
                        mJob = model.getJob();
                    } else {
                        Toast.makeText(mContext, "Some data not received.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, model.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failed(int code, Response<?> response, Throwable t) {
                String msg = "Code: " + String.valueOf(code);
                if (response != null) {
                    msg += " Response: " + response.message();
                } else if (t != null) {
                    msg += " Error: " + t.getMessage();
                }
                Log.e(TAG, "UpdateJobStatus", msg);
                Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

                if (t != null)
                    FirebaseCrash.report(t);

                FirebaseCrash.log(msg);
            }

            @Override
            public void atLast(Call<?> call) {
                stopProgress();
                if (mJob != null)
                    init();
            }
        }));
    }

    //endregion

    //region imp

    private void showUi() {
        ObjectAnimator.ofFloat(mVParent, "alpha", 0, 1).setDuration(1000).start();
    }

    private void init() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(mJob.getJobNo());

        mMilestones.clear();
        mMilestones.addAll(mJob.getMilestones());
        mAdapter.notifyDataSetChanged();

        mTClientName.setText(mJob.getConsignee());
        mTDescription.setText(mJob.getDesc());
        mTHblNumber.setText(mJob.getHblNo());
        mTOrderRefNumber.setText(mJob.getOrderReference());

        if (mJob.getClosed().equals("1")) {
            mTStatus.setText(R.string.text_job_status_closed);
            mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusClosed));
        } else if (mJob.getClosed().equals("0") && mJob.getCurrentStatusId() != null) {
            mTStatus.setText(mJob.getCurrentStatus() != null ? String.valueOf(mJob.getCurrentStatus()) : "Ongoing");
            mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusOngoing));
        } else if (mJob.getClosed().equals("0") && mJob.getCurrentStatusId() == null) {
            mTStatus.setText(R.string.text_job_status_open);
            mTStatus.setTextColor(Functions.GetColor(mContext, R.color.colorTextStatusOpen));
        }

        showUi();
    }

    private void showRemark(final Milestone milestone) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DialogRemarkFragment newFragment = DialogRemarkFragment.newInstance(milestone.getRemarks());
        newFragment.setDialogResultListener(new OnDialogResultListener() {
            @Override
            public void onPositiveResult(Dialog dialog, @Nullable Object value, @Nullable Class type) {
                if (value != null) {
                    updateJobStatus(milestone.getMilestoneId(), "0", (String) value);
                }
                dialog.dismiss();
            }

            @Override
            public void onNegativeResult(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });
        newFragment.show(ft, DialogRemarkFragment.TAG);
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        addContentView(
                getLayoutInflater().inflate(R.layout.layout_progress, null),
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);

        mUser = Preference.getInstance(mContext).getCurrentUser();

        mRVMilestones.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mRVMilestones.setHasFixedSize(true);

        mAdapter = new MilestoneAdapter(mContext, mUser, mMilestones, this);
        mRVMilestones.setAdapter(mAdapter);

        if (getIntent() != null && getIntent().hasExtra(EXTRA_JOB_ID)) {
            mJobId = getIntent().getStringExtra(EXTRA_JOB_ID);
            getJobDetail();
        }
    }

    @Override
    public void onItemClick(View view, int position, Object object) {
        final Milestone milestone = (Milestone) object;

        switch (view.getId()) {
            case R.id.button_delayed:
                showRemark(milestone);
                break;
            case R.id.button_completed:
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                DialogConfirmFragment newFragment = DialogConfirmFragment.newInstance(
                        getString(R.string.dialog_title_confirm), getString(R.string.dialog_message_confirm));
                newFragment.setDialogResultListener(new OnDialogResultListener() {
                    @Override
                    public void onPositiveResult(Dialog dialog, @Nullable Object value, @Nullable Class type) {
                        updateJobStatus(milestone.getMilestoneId(), "1", "");
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeResult(Dialog dialog) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {

                    }
                });
                newFragment.show(ft, DialogConfirmFragment.TAG);
                break;
        }
    }
}
