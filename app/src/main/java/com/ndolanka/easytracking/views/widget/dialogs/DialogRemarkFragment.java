package com.ndolanka.easytracking.views.widget.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.ndolanka.easytracking.R;
import com.ndolanka.easytracking.common.Functions;
import com.ndolanka.easytracking.interfaces.OnDialogResultListener;
import com.ndolanka.easytracking.views.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogRemarkFragment extends AppCompatDialogFragment {
    public static final String TAG = DialogRemarkFragment.class.getSimpleName();
    private static final String ARG_PARAM_REMARK = "param_remark";

    private Context mContext;
    private OnDialogResultListener mListener;
    private String mRemark;

    @BindView(R.id.edit_text_remark)
    EditText mE_Remark;

    public DialogRemarkFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param remarks
     * @return A new instance of fragment DialogRemarkFragment.
     */
    public static DialogRemarkFragment newInstance(String remarks) {
        DialogRemarkFragment fragment = new DialogRemarkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_REMARK, remarks);
        fragment.setArguments(args);
        return fragment;
    }

    public void setDialogResultListener(OnDialogResultListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRemark = getArguments().getString(ARG_PARAM_REMARK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_remark, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mE_Remark.setTextInputLayout(R.id.input_layout_remark);
        mE_Remark.addValidator(Functions.ValidationRequired(mContext));

        if (mRemark != null && !mRemark.isEmpty())
            mE_Remark.setText(mRemark);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //return super.onCreateDialog(savedInstanceState);
        final RelativeLayout root = new RelativeLayout(mContext);
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.button_cancel, R.id.button_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_cancel:
                if (mListener != null) {
                    mListener.onNegativeResult(getDialog());
                }
                break;
            case R.id.button_submit:
                if (mE_Remark.validate()) {
                    if (mListener != null) {
                        mListener.onPositiveResult(getDialog(), mE_Remark.getText().toString(), String.class);
                    }
                }
                break;
        }
    }
}
