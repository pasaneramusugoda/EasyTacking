package com.ndolanka.easytracking.views.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.View;

import com.ndolanka.easytracking.common.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pasan Eramusugoda on 6/3/2016.
 */

public class EditText extends TextInputEditText {

    protected static final String TAG = EditText.class.getSimpleName();
    OnFocusChangeListener focusChangeListener;
    private TextInputLayout textInputLayout;
    private boolean validateOnFocusLost = false;
    private List<ETValidator> validators;

    public EditText(Context context) {
        super(context);

        if (this.isInEditMode())
            return;

        init(context);
    }

    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (this.isInEditMode())
            return;

        init(context);
    }

    public EditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (this.isInEditMode())
            return;

        init(context);
    }

    private void init(Context context) {
        setFont(context);
        // observe the focus state to animate the floating label's text color appropriately
        focusChangeListener = new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (validateOnFocusLost && !hasFocus) {
                    validate();
                }
            }
        };
        super.setOnFocusChangeListener(focusChangeListener);
    }

    public void setTextInputLayout(int resId) {
        try {
            this.textInputLayout = (TextInputLayout) getRootView().findViewById(resId);
        } catch (NullPointerException ex) {
            Log.e(TAG, "setTextInputLayout", ex.getMessage(), ex);
        }
    }

    public boolean isValidateOnFocusLost() {
        return validateOnFocusLost;
    }

    public void setValidateOnFocusLost(boolean validate) {
        this.validateOnFocusLost = validate;
    }

    /**
     * Run validation on a single validator instance
     *
     * @param validator Validator to check
     * @return True if valid, false if not
     */
    public boolean validateWith(@NonNull ETValidator validator) {
        CharSequence text = getText();
        boolean isValid = validator.isValid(text, text.length() == 0);
        if (!isValid) {
            setError(validator.getErrorMessage());
        }
        postInvalidate();
        return isValid;
    }

    /**
     * Check all validators, sets the error text if not
     * <p/>
     * NOTE: this stops at the first validator to report invalid.
     *
     * @return True if all validators pass, false if not
     */
    public boolean validate() {
        if (validators == null || validators.isEmpty()) {
            return true;
        }

        CharSequence text = getText();
        boolean isEmpty = text.length() == 0;

        boolean isValid = true;
        for (ETValidator validator : validators) {
            //noinspection ConstantConditions
            isValid = isValid && validator.isValid(text, isEmpty);
            if (!isValid) {
                this.textInputLayout.setErrorEnabled(true);
                setErrorMessage(validator.getErrorMessage());
                break;
            }
        }
        if (isValid) {
            this.textInputLayout.setErrorEnabled(false);
            setErrorMessage(null);
        }

        postInvalidate();
        return isValid;
    }

    private void setErrorMessage(@Nullable String message) {
        if (textInputLayout == null) {
            setError(message);
        } else {
            textInputLayout.setError(message);
        }
    }

    public boolean hasValidators() {
        return this.validators != null && !this.validators.isEmpty();
    }

    /**
     * Adds a new validator to the View's list of validators
     * <p/>
     * This will be checked with the others in {@link #validate()}
     *
     * @param validator Validator to add
     * @return This instance, for easy chaining
     */
    public EditText addValidator(ETValidator validator) {
        if (validators == null) {
            this.validators = new ArrayList<>();
        }
        this.validators.add(validator);
        return this;
    }

    public void clearValidators() {
        if (this.validators != null) {
            this.validators.clear();
        }
    }

    //region font

    private void setFont(Context context) {
        setFont(context, 0, true);
    }

    private void setFont(Context context, int i, boolean setOnlyDefaultFont) {
        //        Typeface typeface = FontCache.get(Functions.GetFontPath(i), context);
        //        setTypeface(typeface);
    }

    //endregion
}
