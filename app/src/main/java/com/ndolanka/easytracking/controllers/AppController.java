package com.ndolanka.easytracking.controllers;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.ndolanka.easytracking.api.ApiUsage;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Pasan on 10/4/2016.
 */

public class AppController extends Application {

    protected final String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        FirebaseApp.initializeApp(this);
        ApiUsage.initialize(this);
    }
}
